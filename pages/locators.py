from selenium.webdriver.common.by import By

"""Локаторы странице логина"""

class LoginPageLocators:
    login_input = (By.CSS_SELECTOR, "div:nth-child(2) > .p-inputtext")
    password_input = (By.CSS_SELECTOR, "div:nth-child(4) > .p-inputtext")
    login_button = (By.CSS_SELECTOR, ".p-button")
    username = (By.CSS_SELECTOR, ".mat-menu-trigger")
    error_message = (By.CSS_SELECTOR, ".error > div")
    user_lk = (By.CSS_SELECTOR, ".mat-menu-trigger.user")
    exit_button = (By.CSS_SELECTOR, ".user-menu > [routerlinkactive='active-link']")  # '[routerlinkactive]'

"""Локаторы на странице проектов"""

class ProjectPageLocators:
    button_make = (By.CSS_SELECTOR, "div .create-button .p-button")
    make_project = (By.XPATH, "//span[contains(text(), 'Проект')]")
    make_folder = (By.XPATH, "//span[contains(text(), 'Папку')]")
    input_proj_name = (
    By.CSS_SELECTOR, "input.p-inputtext.p-component.ng-pristine")  # это плохой локатор, ID меняется с каждым вызовом окна
    button_make_proj = (By.CSS_SELECTOR, "button.mat-raised-button")
    project_card = (By.CSS_SELECTOR, ".cards-container>.ng-star-inserted")  # первый попавшийся в последних проектах
    projects_tree = (By.CSS_SELECTOR, "div.node-content-wrapper>tree-node-content>.ng-star-inserted")
    dop_menu_card = (By.CSS_SELECTOR, ".pi.pi-ellipsis-v")
    dop_menu_card_delete = (By.CSS_SELECTOR, ".p-menuitem-icon.pi-trash")
    dop_menu_card_rename = (By.CSS_SELECTOR, ".pi-pencil")
    dop_menu_card_copy = (By.CSS_SELECTOR, ".pi-copy")
    dop_menu_card_replace = (By.CSS_SELECTOR, ".pi-arrow-circle-right")
    button_rename = (By.CSS_SELECTOR, ".mat-raised-button.mat-primary")
    button_replace = (By.CSS_SELECTOR, ".mat-primary>.mat-button-wrapper")
    project_name = (By.CSS_SELECTOR, ".p-col.project-name")
    folder_name_text = (By.CSS_SELECTOR, "tree-node-wrapper:nth-child(1) tree-node-content > .ng-star-inserted")
    folder_in_tree = (By.XPATH, "//tree-node-content/span[contains(text(), 'test_folder')]")
    folder_in_tree_subfolder = (By.XPATH, "//tree-node-content/span[contains(text(), 'subfolder')]")
    dop_menu_folder = (By.CSS_SELECTOR, ".pi.pi-ellipsis-h.node-icon.ng-star-inserted")
    dop_menu_folder_create_folder = (By.CSS_SELECTOR, ".p-menu .pi-folder")
    dop_menu_folder_delete = (By.CSS_SELECTOR, ".p-menuitem-icon.pi-trash")
    dop_menu_folder_create_proj = (By.CSS_SELECTOR, ".pi-file")
    dop_menu_folder_rename = (By.CSS_SELECTOR, ".pi-pencil")
    folder_name_in_page = (By.CSS_SELECTOR, ".group-name")
    project_card_name = (By.CSS_SELECTOR, ".p-col.project-name")
    last_project_text_in_page = (By.XPATH, "//div[contains(text(), 'Последние проекты')]")
    input_folder_name_search = (By.CSS_SELECTOR, "[placeholder='Выберите папку']")
    folder_name_search_list = (By.XPATH, "//li/span[contains(text(), 'test_folder')]")
    exigner = (By.CSS_SELECTOR, "[alt='exigner']")
    search_field = (By.CSS_SELECTOR, "[role='searchbox']")

"""Локаторы элементов в тулбаре конструктора"""

class ConstructorPageToolbarLocators:
    toolbar_tree_of_evt = (By.CSS_SELECTOR, '[alt="Tree of Everything"]')
    toolbar_select = (By.CSS_SELECTOR, '[alt="select"]')
    toolbar_text = (By.CSS_SELECTOR, '[alt="Text"]')
    toolbar_rect = (By.CSS_SELECTOR, '[alt="Rectangle"]')
    toolbar_triangle = (By.CSS_SELECTOR, '[alt="Triangle"]')
    toolbar_ellipse = (By.CSS_SELECTOR, '[alt="Ellipse"]')
    toolbar_image = (By.CSS_SELECTOR, '[alt="Image"]')
    toolbar_tb = (By.CSS_SELECTOR, '[alt="Toolbar"]')
    toolbar_package = (By.CSS_SELECTOR, '[alt="Package"]')

"""Локаторы элементов в дереве всего"""

class ConstructorPageToELocators:
    expand_project_elements = (By.CSS_SELECTOR, "app-tree-graph span>.toggle-children")
    button_delete_t_o_e = (By.CSS_SELECTOR, "[mattooltip='Удалить']")
    expand_html_elements = (By.CSS_SELECTOR, "app-tree-html-builder span>.toggle-children")
    # html_button = (By.XPATH, "//span[contains(text(), 'Button')]")
    # html_button = (By.CSS_SELECTOR, "tree-node-children>div>tree-node-collection>div>tree-node:nth-child(1)>div>tree-node-wrapper>.node-wrapper>.node-content-wrapper>tree-node-content")
    html_button = (By.CSS_SELECTOR, "body > app-root > div > app-graph-editor > mat-drawer-container > mat-drawer-content > app-widget-graph-editor > div > div.ng-star-inserted > app-tree-of-everything > div > div.body > app-tree-html-builder > tree-root > tree-viewport > div > div > tree-node-collection > div > tree-node > div > tree-node-children > div > tree-node-collection > div > tree-node:nth-child(1) > div > tree-node-wrapper > div > div > tree-node-content > span")
    html_div = (By.XPATH, "//span[contains(text(), 'Div')]")
    html_img = (By.XPATH, "//span[contains(text(), 'Img')]")
    html_input = (By.XPATH, "//span[contains(text(), 'Input')]")
    html_checkbox = (By.XPATH, "//span[contains(text(), 'Checkbox')]")
    html_switch = (By.XPATH, "//span[contains(text(), 'Switch')]")
    html_select = (By.XPATH, "//span[contains(text(), 'Select')]")
    html_select2 = (By.XPATH, "//span[contains(text(), 'Select2')]")
    html_slider = (By.XPATH, "//span[contains(text(), 'Slider')]")
    html_label = (By.XPATH, "//span[contains(text(), 'Label')]")
    html_link = (By.XPATH, "//span[contains(text(), 'Link')]")
    html_svg = (By.XPATH, "//span[contains(text(), 'SVG')]")
    html_iframe = (By.XPATH, "//span[contains(text(), 'Iframe')]")
    html_angular_templ = (By.XPATH, "//span[contains(text(), 'Angular Template')]")

"""Локаторы для страницы конструктора"""

class ConstructorPageLocators:
    easy_logic_button = (By.CSS_SELECTOR, ".easy-logic>.icon-button")
    deploy_button = (By.CSS_SELECTOR, ".build-buttons>.build-shedule")
    deploy_button_browser = (By.CSS_SELECTOR, ".build-buttons>.deploy")
    pkg_in_workspace = (By.CSS_SELECTOR, "[label='Package']>path:nth-child(3)")
    pkg_in_workspace_2 = (By.CSS_SELECTOR, "[label='Package']>path:nth-child(1)")
    graph = (By.CSS_SELECTOR, "#graph > svg")
    success_deploy_message = (By.CSS_SELECTOR, ".toast-success")
    platform_brw_folder = (By.CSS_SELECTOR, "tree-node:nth-child(1) tree-node-content>span")
    html_5_icon = (By.CSS_SELECTOR, 'g:nth-child(5)')
    property_panel = (By.CSS_SELECTOR, "app-widget-graph-properties .container")
    rect_in_ws = (By.CSS_SELECTOR, "g:nth-child(7)>rect")
    rect_in_ws_1 = (By.CSS_SELECTOR, "g:nth-child(1)>rect")


class ContextMenuLocators:
    cut = (By.CSS_SELECTOR, ".mxPopupMenu tr:nth-child(1)")
    copy = (By.CSS_SELECTOR, ".mxPopupMenu tr:nth-child(2)")
    paste = (By.CSS_SELECTOR, ".mxPopupMenu tr:nth-child(3)")
    delete = (By.CSS_SELECTOR, ".mxPopupMenu tr:nth-child(5)")
    save_to_server = (By.CSS_SELECTOR, ".mxPopupMenu tr:nth-child(6)")


"""Локаторы для панели свойств прямоугольника,треугольника, эллипса """

class PropertyPanelLocatorsRTE:
    id = (By.CSS_SELECTOR, "app-widget-graph-properties div>div:nth-child(2) input")
    label = (By.CSS_SELECTOR, "app-widget-graph-properties div>div:nth-child(3) input")
    name = (By.CSS_SELECTOR, "app-widget-graph-properties div>div:nth-child(4) input")
    coord_x = (By.CSS_SELECTOR, "div:nth-child(9) > app-widget-geometry-properties input")
    coord_y = (By.CSS_SELECTOR, "div:nth-child(10) > app-widget-geometry-properties input")
    width_el = (By.CSS_SELECTOR, "div:nth-child(8) > app-widget-geometry-properties input")
    high_el = (By.CSS_SELECTOR, "div:nth-child(7) > app-widget-geometry-properties input")
    fon = (By.CSS_SELECTOR, "div:nth-child(12) app-widget-style-properties input")
    size_header = (By.CSS_SELECTOR, "div:nth-child(13) app-widget-style-properties input")
    color_border = (By.CSS_SELECTOR, "div:nth-child(14) app-widget-style-properties input")
    thickness_border = (By.CSS_SELECTOR, "div:nth-child(15) app-widget-style-properties input")
    hex_input = (By.CSS_SELECTOR, ".hex-text>.box>input")

"""Локаторы элементов в режиме EasyLogic"""

class EasyLogicPageLocators:
    exit_easy_logic_button = (By.CSS_SELECTOR, "[mattooltip='Exit easyLogic mode']")
    toolbar_connect = (By.CSS_SELECTOR, '[alt="connect"]')
    toolbar_start = (By.CSS_SELECTOR, '[alt="Start"]')
    toolbar_state = (By.CSS_SELECTOR, '[alt="State"]')
    toolbar_action = (By.CSS_SELECTOR, '[alt="Action"]')
    toolbar_custom = (By.CSS_SELECTOR, '[alt="Custom"]')
    toolbar_condition = (By.CSS_SELECTOR, '[alt="Condition"]')
    toolbar_event = (By.CSS_SELECTOR, '[alt="Event"]')
    toolbar_inner_event = (By.CSS_SELECTOR, '[alt="InnerEvent"]')
    toolbar_stop = (By.CSS_SELECTOR, '[alt="Stop"]')
    start_in_page = (By.CSS_SELECTOR, 'g[label="Start"]')
    # локаторы портов старт и action будут верными олько при последовательном добавлении блоков
    port_of_start = (By.CSS_SELECTOR, 'svg>g g>g:nth-of-type(3)')
    port_of_action = (By.CSS_SELECTOR, 'svg>g g>g:nth-of-type(8)')

    state_in_page = (By.CSS_SELECTOR, 'g[label="State"]')
    action_in_page = (By.CSS_SELECTOR, 'g[label="Action"]')
    custom_in_page = (By.CSS_SELECTOR, 'g[label="Custom block"]')
    add_var = (By.CSS_SELECTOR, ".p-col-2")
    input_var_name = (By.CSS_SELECTOR, "app-widget-var-properties div:nth-child(1)>input")
    input_var_value = (By.CSS_SELECTOR, "div:nth-child(2)>input")
    button_var_ok = (By.CSS_SELECTOR, ".p-button-label")
    script_editor = (By.CSS_SELECTOR, ".ace_editor>.ace_text-input")
    connect_in_page = (By.CSS_SELECTOR, "g>g>path:nth-child(2)")
