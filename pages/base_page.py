import time

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from pytest_html_reporter import attach


class BasePage:
    def __init__(self, browser, url, timeout=3):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)
        """Добавление скриншотов в html-report при падении теста"""
        # attach(data=browser.get_screenshot_as_png())

    def open(self):
        self.browser.maximize_window()
        self.browser.get(self.url)

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    # проверяем, что элемент появляется на странице в течение заданного времени:
    def is_element_present_timeout(self, how, what, timeout=7):
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException).until(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return False
        return True

    # проверяем,что какой-то элемент исчезает
    def is_disappeared(self, how, what, timeout=4):
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException). \
                until_not(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return False

        return True

    # проверяем, что какой-то элемент стал кликабельным
    def is_element_to_be_clickable(self, how, what, timeout=3):
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException).until(EC.element_to_be_clickable((how, what)))
        except TimeoutException:
            return False
        return True

    # проверяем, что в ссылке появилась нужная подстрока
    def is_url_contains(self, url, timeout=3):
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException).until(EC.url_changes(url))
        except TimeoutException:
            return False
        return True

    # метод, для симуляции drag-n-drop с помощью js-скрипта
    def drag_and_drop(self, source, target=None, offsetX=0, offsetY=0, delay=25, key=None):
        # load drag and drop
        with open("dnd.js") as f:
            drag_and_drop_js = f.read()
        self.browser.execute_script(drag_and_drop_js, source, target, offsetX, offsetY, delay, key)
        time.sleep(1)
