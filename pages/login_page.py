from pages.base_page import BasePage
from pages.locators import LoginPageLocators, ProjectPageLocators
import time
import allure

login = "ngtest"
password = "OImdD5KTv5"
login_at = "ng-autotest"
password_at = "Pass_2684!"
login_inv = "trun@1,"
password_inv = "!@#$%^&*()"
username = "Тест Тестович"
username_at = "Тест Тестовый"
error_message = "Неправильное имя пользователя или пароль"


class LoginPage(BasePage):
    def should_be_login_page(self):
        self.should_be_login_url()
        self.should_be_login_input()
        self.should_be_password_input()
        self.should_be_button_login_disable()
        print('Форма логина пристутствует')

    # проверяем, что в ссылке есть login
    @allure.step("Проверяем, что в ссылке есть login")
    def should_be_login_url(self):
        assert "login" in self.browser.current_url, "Link not increase 'login'"

    # проверяем, что есть поле ввода логина
    @allure.step("Проверяем, что есть поле ввода логина")
    def should_be_login_input(self):
        assert self.is_element_present(*LoginPageLocators.login_input), "Login input is not presented"

    # проверяем, что есть поле ввода пароля
    @allure.step("Проверяем, что есть поле ввода пароля")
    def should_be_password_input(self):
        assert self.is_element_present(*LoginPageLocators.password_input), "Password input is not presented"

    # проверяем, что кнопка логина не доступна при незаполненных полях
    def should_be_button_login_disable(self):
        login_button = self.browser.find_element(*LoginPageLocators.login_button)
        login_button_disable = login_button.get_attribute("disabled")
        assert login_button_disable == "true", "Кнопка доступна, хотя ничего не введено!"
        print('Кнопка Войти не доступна при незаполненных полях')

    # Авторизация при неверных логин\пароль
    def cant_be_user_authorization(self):
        # ищем поле логина
        input_login = self.browser.find_element(*LoginPageLocators.login_input)
        # и вводим неверный логин
        input_login.send_keys(login_inv)
        # ищем поле пароля
        input_password = self.browser.find_element(*LoginPageLocators.password_input)
        # и вводим неверный пароль
        input_password.send_keys(password_inv)
        # time.sleep(1)
        # нажимаем кнопку Войти
        self.browser.find_element(*LoginPageLocators.login_button).click()
        time.sleep(1)
        print('Авторизация при неверных УД не выполнена')

    # Попытка авторизации при пустом логине - введенном пароле
    def cant_be_user_authorization_empty_login_fill_pass(self):
        # ищем поле пароля
        input_password = self.browser.find_element(*LoginPageLocators.password_input)
        # и вводим неверный пароль
        input_password.send_keys(password_inv)
        # time.sleep(2)
        print('Авторизация при пустом логине, введенном пароле - недоступна')

    # Попытка авторизации при путосм пароле - введенном логине (верном)
    def cant_be_user_authorization_empty_pass_fill_login(self):
        # ищем поле логина
        input_login = self.browser.find_element(*LoginPageLocators.login_input)
        # и вводим верный логин
        input_login.send_keys(login_at)
        # time.sleep(2)
        print('Авторизация при пустом пароле, введенном логине - недоступна')

    # Проверяем, что вход не удался (сообщение об ошибке)
    def should_be_error_message_login(self):
        # проверяем, появилось ли сообщение об ошибке
        assert self.is_element_present(*LoginPageLocators.error_message), "Error message not found!"
        # сравниваем текст ошибки и сравниваем с эталонным
        error_message_find = self.browser.find_element(*LoginPageLocators.error_message).text
        assert error_message == error_message_find, "Сообщение об ошибке не верное"
        print('Сообщение о невреном логин/пароле присутствует')

    # Авторизация под существующим юзером
    def can_be_user_authorization(self):
        # ищем поле логина и вводим его
        with allure.step("Вводим логин"):
            input_login = self.browser.find_element(*LoginPageLocators.login_input)
        input_login.send_keys(login_at)
        # ищем поле пароля и вводим его
        with allure.step("Вводим пароль"):
            input_password = self.browser.find_element(*LoginPageLocators.password_input)
        input_password.send_keys(password_at)
        # time.sleep(1)
        # нажимаем кнопку Войти
        with allure.step("Нажимаем Войти"):
            self.browser.find_element(*LoginPageLocators.login_button).click()
        if self.is_element_present(*LoginPageLocators.error_message):
            raise Exception("Incorrect login data!")

    # Проверяем, что загрузилась страница с последними проектами
    def should_be_see_last_project_in_page(self):
        last_project_text_in_page = self.browser.find_element(*ProjectPageLocators.last_project_text_in_page).text
        assert last_project_text_in_page == 'Последние проекты', "Открылась страница не с последними проектами, " \
                                                                 "либо отсутствует текст 'Последние проекты'"

    # Проверяем, что зашли под тем пользователем, которым хотели))
    def should_be_username_correct(self):
        # ищем логин пользователя и сравниваем с тем, что должен быть
        username_lk = self.browser.find_element(*LoginPageLocators.username).text
        assert username_at in username_lk, "Username is wrong"  # это хрень, надо сделать на полное совпадение
        print('Авторизация под валидным юзером успешна')

    # Разлогиниваемся
    def can_be_exit_from_lk(self):
        # нажимаем на имя юзера
        with allure.step("Нажимаем на имя юзера"):
            self.browser.find_element(*LoginPageLocators.user_lk).click()
        # ждем, когда Выход станет кликабельным
        # self.is_element_to_be_clickable(*LoginPageLocators.exit_button)
        # жмем Выход
        with allure.step("Нажимаем на Выход"):
            self.browser.find_element(*LoginPageLocators.exit_button).click()
        time.sleep(0.5)
        print('Выход из приложения успешен')
