import pytest
import requests
import allure
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url, params=params, data=data, json=json, headers=headers)

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url, params=params, headers=headers)

@pytest.fixture
def dog_api():
    return ApiClient(base_address="https://dog.ceo/api/")

"""Фикстура для запуска браузера в moon c GUI"""
@pytest.fixture(scope="function")
def browser():
    options = webdriver.ChromeOptions()
    print("\nstart chrome browser for test..")
    options.set_capability("browserName", "chrome")
    options.set_capability("version", "87.0")
    options.set_capability("enableVNC", True)
    browser = webdriver.Remote(
        command_executor='http://10.1.131.23:4444/wd/hub', options=options)
    yield browser
    print("\nquit browser..")
    browser.quit()
