import time
import pytest
from pages.login_page import LoginPage
import allure
from allure_commons.types import AttachmentType
link = "https://master.boquar.tech/"


@pytest.mark.smoke
@allure.feature('Login Page')
class TestLogin:
    @allure.story('Авторизация под валидным юзером')
    @allure.severity('blocker')
    def test_valid_login(self, browser):
        page = LoginPage(browser, link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
        with allure.step(f"Открываем страницу {link}"):
            page.open()
        with allure.step("Проверяем, что на странице есть форма логина"):
            page.should_be_login_page()
        with allure.step("Пытаемся авторизоваться под валидным юзером"):
            page.can_be_user_authorization()
        with allure.step("Проверяем, что в приложении имя юзера корректное"):
            page.should_be_username_correct()
        with allure.step("Проверяем, что открылась страница с последними проектами"):
            page.should_be_see_last_project_in_page()

    @allure.story('Авторизация под невалидным юзером')
    @allure.severity('blocker')
    def test_invalid_login_pass(self, browser):
        page = LoginPage(browser, link)
        with allure.step(f"Открываем страницу {link}"):
            page.open()
        with allure.step("Пытаемся авторизоваться под невалидным юзером"):
            page.cant_be_user_authorization()
        with allure.step("Проверяем, что вход не удался и есть сообщение о неверных логин-пароле"):
            page.should_be_error_message_login()

    @allure.story('Авторизация при незаполненном логине, но верном пароле')
    @allure.severity('blocker')
    def test_empty_login_fill_pass(self, browser):
        page = LoginPage(browser, link)
        with allure.step(f"Открываем страницу {link}"):
            page.open()
        with allure.step("Попытка войти при пустом логине, верном пароле"):
            page.cant_be_user_authorization_empty_login_fill_pass()
        with allure.step("Проверяем, что кнопка 'Войти' недоступна"):
            page.should_be_button_login_disable()

    @allure.story('Авторизация при незаполненном пароле, но верном логине')
    @allure.severity('blocker')
    def test_empty_pass_fill_login(self, browser):
        page = LoginPage(browser, link)
        with allure.step(f"Открываем страницу {link}"):
            page.open()
        with allure.step("Попытка войти при пустом пароле, верном логине"):
            page.cant_be_user_authorization_empty_pass_fill_login()
        with allure.step("Проверяем, что кнопка 'Войти' недоступна"):
            page.should_be_button_login_disable()

    @allure.story('Выход из приложения')
    @allure.severity('critical')
    def test_exit_from_lk(self, browser):
        page = LoginPage(browser, link)
        with allure.step(f"Открываем страницу {link}"):
            page.open()
        with allure.step("Пытаемся авторизоваться под валидным юзером"):
            page.can_be_user_authorization()
        with allure.step("Выходим из приложения"):
            page.can_be_exit_from_lk()
        with allure.step("Проверяем, что вышли на страницу логина"):
            page.should_be_login_page()
